# Ansible role init Debian

Ansible role to prepare an debian servers in my self-hosting context.

It was initially targeted for Olinuxino A64 boards, but has been generalized since then.

It works on Debian >=10, but also on Ubuntu >=24.04

It does the following :
*  Low-level system configuration :
    *  Set the hostname
    *  Change the timezone
    *  Prohibit root SSH user/password authentication
    *  Add French locale
    *  Install a few packages I commonly use
*  Install and configure Exim
*  Install and configure Rkhunter
*  Install and configure Chkrootkit
*  Install and configure Munin
*  Install and configure Fail2ban

Tags can be used to run only a sub-part of the playbook.

Several variables need to be set.

Example:

```
      custome_hostname: myhostname
      init_debian_email_for_notifications: mossroy@mossroy.fr
      init_debian_sender_email_for_notifications: sender@mossroy.fr
      init_debian_regular_user: mossroy
      init_debian_fail2ban_freemobilesms_user: 123456789
      init_debian_fail2ban_freemobilesms_password: mypassword
      init_debian_fail2ban_ovhsms_account: sms-mb123456-1
      init_debian_fail2ban_ovhsms_login: mylogin
      init_debian_fail2ban_ovhsms_password: mypassword
      init_debian_fail2ban_ovhsms_from: "MOSSROY"
      init_debian_fail2ban_ovhsms_to: 0033612345678
      init_debian_fail2ban_ovhsms_extraparams: "&noStop=1"
      init_debian_exim4_passwd_client: "*:sender@mossroy.fr:password"
      init_debian_domain_name: mossroy.fr
      init_debian_exim4_smarthost: "mail.gandi.net:587"
      init_debian_munin_node_lineinfile_regexp: '^allow \^192\\\.168\\\.'
      init_debian_munin_node_lineinfile_line: 'allow ^192\.168\.0\..+$'
```
