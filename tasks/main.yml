- name: Set hostname (1/5)
  hostname:
    name: "{{custom_hostname}}"
- name: Set hostname (2/5)
  lineinfile:
    path: /etc/hosts
    regexp: '^127\.0\.0\.1.*(olinuxino|lime|localhost)'
    line: "127.0.0.1   localhost"
- name: Set hostname (3/5)
  lineinfile:
    path: /etc/hosts
    regexp: '^127\.0\.1\.1'
    line: "127.0.1.1   {{custom_hostname}}"
- name: Set hostname (4/5)
  lineinfile:
    path: /etc/hosts
    regexp: '^::1 .*(olinuxino|lime|localhost)'
    line: "::1         localhost ipv-localhost ip6-loopback"
- name: Set hostname (5/5)
  lineinfile:
    path: /etc/hosts
    regexp: '^::1:1'
    line: "::1:1       {{custom_hostname}}"
- name: Set timezone to Europe/Paris
  timezone:
    name: Europe/Paris
  # TODO does not work on Ubuntu 24.04, and might be useless?
  #notify: restart rsyslog
- name: Prohibit root SSH user/password authentication
  lineinfile:
    path: /etc/ssh/sshd_config
    regexp: '^PermitRootLogin '
    line: 'PermitRootLogin prohibit-password'
  notify: restart sshd
- name: remove user olimex if it exists
  user:
    name: olimex
    state: absent
- name: install bash-completion, cron, man-db, rsync, locales, aptitude, apt-transport-https, unattended-upgrades, ntp and cryptsetup
  apt:
    name: ['bash-completion','cron','man-db','rsync','locales','aptitude','apt-transport-https','unattended-upgrades','apt-listchanges','cryptsetup','busybox','ntp']
    state: present
- name: add locale FR
  locale_gen:
    name: fr_FR.UTF-8
- name: install glances if package available
  apt:
    name: glances
    state: present
  when: not (ansible_distribution == 'Debian' and ansible_distribution_major_version == '11')
- name: install pip if glances package is not available
  apt:
    name: python3-pip
  when: ansible_distribution == 'Debian' and ansible_distribution_major_version == '11'
- name: install glances through pip if its package is not available
  pip:
    name: glances
  when: ansible_distribution == 'Debian' and ansible_distribution_major_version == '11'
- name: workaround for disk IOs displayed in glances
  lineinfile:
    path: /usr/lib/python3/dist-packages/psutil/_pslinux.py
    regexp: '^            elif flen == 14 or flen .= 18'
    line: '            elif flen == 14 or flen >= 18:  #Hotfix for kernel>=5.5. See https://github.com/giampaolo/psutil/commit/2e0952e939d6ab517449314876d8d3488ba5b98b#diff-befef210ffc1d19a84ee5969886021a3'
    state: present
  when: ansible_kernel is version('5.5','>=') and ansible_distribution == 'Debian' and ansible_distribution_major_version == '10'
- name: cron to check all services started properly after reboot
  cron:
    name: check all services started properly after reboot
    special_time: reboot
    user: root
    job: "/bin/sleep 60; systemctl --failed | grep failed"
    cron_file: check_all_services_started_properly_after_reboot
  tags:
  - systemctlfailed
- name: install exim4
  apt:
    name: ['exim4-daemon-light','mailutils']
    state: present
  tags:
  - exim
- name: configure exim4
  template:
    src: exim4/update-exim4.conf.conf
    dest: /etc/exim4/update-exim4.conf.conf
    owner: root
    group: root
    mode: u=rw,g=r,o=r
  notify: update exim4 conf
  tags:
  - exim
- name: configure SMTP authentication
  template:
    src: exim4/passwd.client
    dest: /etc/exim4/passwd.client
    owner: root
    group: Debian-exim
    mode: u=rw,g=r,o=
  notify: restart exim4
  tags:
  - exim
- name: configure email aliases
  template:
    src: exim4/aliases
    dest: /etc/aliases
    owner: root
    group: root
    mode: u=rw,g=r,o=r
  notify: update email aliases
  tags:
  - exim
- name: configure email addresses
  template:
    src: exim4/email-addresses
    dest: /etc/email-addresses
    owner: root
    group: root
    mode: u=rw,g=r,o=r
  tags:
  - exim
- name: install rkhunter
  apt:
    name: rkhunter
    state: present
  notify:
    - update rkhunter database
    - update rkhunter props
  tags:
  - rkhunter
- name: configure rkhunter (1/4)
  lineinfile:
    path: /etc/default/rkhunter
    regexp: '^CRON_DAILY_RUN='
    line: 'CRON_DAILY_RUN="true"'
  tags:
  - rkhunter
- name: configure rkhunter (2/4)
  lineinfile:
    path: /etc/default/rkhunter
    regexp: '^CRON_DB_UPDATE='
    line: 'CRON_DB_UPDATE="true"'
  tags:
  - rkhunter
- name: configure rkhunter (3/4)
  template:
    src: rkhunter/rkhunter.conf.local
    dest: /etc/rkhunter.conf.local
    owner: root
    group: root
    mode: u=rw,g=r,o=r
  tags:
  - rkhunter
- name: configure rkhunter (4/4)
  # I found no other solution, else it detects this file in apache2 docker containers. See https://bugs.archlinux.org/task/63369
  lineinfile:
    path: /usr/bin/rkhunter
    regexp: "^[ \t]*libkeyutils.so.1.9:Spam tool component$"
    state: absent
  tags:
  - rkhunter
- name: set default init_debian_chkrootkit_conf_location variable value
  set_fact:
    init_debian_chkrootkit_conf_location: /etc/chkrootkit
  tags:
  - chkrootkit
- name: set init_debian_chkrootkit_conf_location variable value on old debian
  set_fact:
    init_debian_chkrootkit_conf_location: /etc
  when: ansible_distribution == 'Debian' and ansible_distribution_major_version <= '11'
  tags:
  - chkrootkit
- name: set default init_debian_chkrootkit_daily_executable variable value
  set_fact:
    init_debian_chkrootkit_daily_executable: /usr/sbin/chkrootkit-daily
  tags:
  - chkrootkit
- name: set init_debian_chkrootkit_daily_executable variable value on old debian
  set_fact:
    init_debian_chkrootkit_daily_executable: /etc/cron.daily/chkrootkit
  when: ansible_distribution == 'Debian' and ansible_distribution_major_version <= '11'
  tags:
  - chkrootkit

- name: install chkrootkit
  apt:
    name: chkrootkit
    state: present
  notify:
    - update chkrootkit database
  tags:
  - chkrootkit
- name: configure chkrootkit (1/5)
  lineinfile:
    path: "{{init_debian_chkrootkit_conf_location}}/chkrootkit.conf"
    regexp: '^RUN_DAILY='
    line: 'RUN_DAILY="true"'
  tags:
  - chkrootkit
- name: configure chkrootkit (2/5)
  lineinfile:
    path: "{{init_debian_chkrootkit_conf_location}}/chkrootkit.conf"
    regexp: '^DIFF_MODE='
    line: 'DIFF_MODE="true"'
  tags:
  - chkrootkit
- name: configure chkrootkit (3/5)
  template:
    src: chkrootkit/chkrootkit.ignore
    dest: "{{init_debian_chkrootkit_conf_location}}/"
  tags:
  - chkrootkit
- name: configure chkrootkit (4/5)
  lineinfile:
    path: /etc/cron.daily/chkrootkit
    regexp: '^IGNORE_FILE='
    line: "IGNORE_FILE={{init_debian_chkrootkit_conf_location}}/chkrootkit.ignore"
    insertafter: '^CF='
  tags:
  - chkrootkit
- name: configure chkrootkit (5/5)
  replace:
    path: /etc/cron.daily/chkrootkit
    regexp: 'eval \$CHKROOTKIT \$RUN_DAILY_OPTS \|'
    replace: 'eval $CHKROOTKIT $RUN_DAILY_OPTS 2>&1 |'
  tags:
  - chkrootkit
- name: install munin-node
  apt:
    name: ['munin-node','libcache-cache-perl','libwww-perl']
    state: present
  tags:
  - munin
- name: configure munin-node (1/2)
  lineinfile:
    path: /etc/munin/munin-node.conf
    insertafter: '^allow \^::1'
    regexp: "{{init_debian_munin_node_lineinfile_regexp}}"
    line: "{{init_debian_munin_node_lineinfile_line}}"
  notify: restart munin-node
  tags:
  - munin
- name: configure munin-node (2/2)
  lineinfile:
    path: /etc/munin/munin-node.conf
    insertafter: '^allow \^::1'
    regexp: "{{init_debian_munin_node_lineinfile2_regexp}}"
    line: "{{init_debian_munin_node_lineinfile2_line}}"
  notify: restart munin-node
  tags:
  - munin
- name: disable some useless munin-node plugins
  file:
    path: "/etc/munin/plugins/{{ item }}"
    state: absent
  loop:
    - nfs4_client
    - nfs_client
    - nfsd4
    - nfsd
    - ntp_kernel_err
    - ntp_kernel_pll_freq
    - ntp_kernel_pll_off
    - ntp_offset
  tags:
  - munin
- name: install fail2ban
  apt:
    name: ['fail2ban', 'whois']
    state: present
  tags:
  - fail2ban
- name: configure fail2ban
  template:
    src: fail2ban/jail.local
    dest: /etc/fail2ban/jail.local
    owner: root
    group: root
    mode: u=rw,g=r,o=r
  notify: restart fail2ban
  tags:
  - fail2ban
- name: add or modify fail2ban filters
  template:
    src: fail2ban/filter.d/{{item}}
    dest: /etc/fail2ban/filter.d/{{item}}
    owner: root
    group: root
    mode: u=rw,g=r,o=r
  loop:
    - nextcloud.conf
    - antispam-bee.conf
    - tt-rss-auth.conf
  notify: restart fail2ban
  tags:
  - fail2ban
- name: add or modify fail2ban actions
  template:
    src: fail2ban/action.d/{{item}}
    dest: /etc/fail2ban/action.d/{{item}}
    owner: root
    group: root
    mode: u=rw,g=r,o=r
  loop:
    - mail-whois-lines.local
    - sendmail-common.local
    - sendSMSFreeMobile.conf
    - sendSMSFreeMobileOnStart.conf
    - sendSMSOVH.conf
    - sendSMSOVHOnStart.conf
  notify: restart fail2ban
  tags:
  - fail2ban
- name: enable some munin plugins for Fail2ban
  file:
    dest: "/etc/munin/plugins/{{ item }}"
    src: "/usr/share/munin/plugins/{{ item }}"
    state: link
  loop:
    - fail2ban
  notify: restart munin-node
  tags:
  - fail2ban
  - munin
- name: add workaround for timejump of A64 CPUs
  template:
    src: fix-date-after-a64-timejump/fix-date-after-a64-timejump
    dest: /etc/cron.hourly/
    owner: root
    group: root
    mode: u=rwx,g=r,o=r
  when: ansible_architecture == "aarch64"
  tags:
  - timejump
