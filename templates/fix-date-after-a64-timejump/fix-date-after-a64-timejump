#!/bin/sh
#
# Trivial script to save the current date to a file, and check afterwards if the
# date did not suddenly change to several decades in the future.
# If it happens, it reverts the date to the previously stored one.
# It is supposed to be executed  regulary (like every hour).
#
# This is a dirty workaround for the hardware "timejump" issue of AllWinner A64
# CPUs.
# Reverting the date avoids loosing network connectivity for headless servers.
#
# This is strongly inspired from the work of Steve McIntyre on fake-hw-clock:
# https://git.einval.com/cgi-bin/gitweb.cgi?p=fake-hwclock.git
# Copyright 2012-2016 Steve McIntyre <93sam@debian.org>
# and 2022 Mossroy <mossroy@mossroy.fr>
#
# License: GPLv2

if [ "$FILE"x = ""x ] ; then
    #FILE=/etc/recent-datetime-stored-to-check-for-a64-timejump.data
    FILE=/etc/datetime-to-check-for-a64-timejump.data
fi

# Midnight on the day of this release, used as a sanity check when
# saving
HWCLOCK_EPOCH="2022-01-28 00:00:00"
HWCLOCK_EPOCH_SEC="1643328000"

# Number of seconds for 90 years
NINETY_YEARS_SEC="2840184000"

FORCE=false
if [ "$1"x = "force"x ] ; then
    FORCE=true
fi

# Check for a timejump
if [ -e $FILE ] ; then
    SAVED="$(cat $FILE)"
    SAVED_SEC=$(date -u -d "$SAVED" '+%s')
    SAVED_PLUS_NINETY_YEARS_SEC=$(expr $SAVED_SEC + $NINETY_YEARS_SEC)
    NOW_SEC=$(date -u '+%s')
    if $FORCE || [ $NOW_SEC -ge $SAVED_PLUS_NINETY_YEARS_SEC ] ; then
        echo "Current date ($(date -u '+%Y-%m-%d %H:%M:%S')) is more than 90 years later than saved date ($SAVED)"
        echo "This is probably due to the hardware timejump issue of your A64 CPU"
        echo "We now revert to the previously saved date, to avoid loosing network connectivity"
        date -u -s "$SAVED"
    fi
fi

# Update the stored date
NOW_SEC=$(date -u '+%s')
if $FORCE || [ $NOW_SEC -ge $HWCLOCK_EPOCH_SEC ] ; then
    date -u '+%Y-%m-%d %H:%M:%S' > $FILE
else
    echo "Incorrect date/time detected!"
    echo "fix-date-after-a64-timejump release date is in the future: $HWCLOCK_EPOCH"
    echo "Current system time: $(date -u '+%Y-%m-%d %H:%M:%S')"
    echo "To force the saved system clock backwards in time anyway, use \"force\""
fi
